<?php// リクエスト処理$name = htmlspecialchars($_POST['name']);$email = htmlspecialchars($_POST['email']);
$address = htmlspecialchars($_POST['address']);
$phone = htmlspecialchars($_POST['phone']);
$company = htmlspecialchars($_POST['company']);
$position = htmlspecialchars($_POST['position']);
$cambodia = $_POST['cambodia'];
$buget = htmlspecialchars($_POST['buget']);$experience = $_POST['experience'];$comment = htmlspecialchars($_POST['comment'], ENT_QUOTES, 'UTF-8');// エンコードの設定mb_language("Japanese");mb_internal_encoding("UTF-8");
// 送信者の設定$to = "asset-design@annacampartners.com";//$to = "h-sato@beniten.com";   
$header = "From: ". $email;// 件名、内容の設定$subject = "[カンボジア不動産投資コラボレーション企画]お問い合わせ";	$body = "***** [カンボジア不動産投資コラボレーション企画]から問い合わせを受付ました。*****\n\n";	$body .= "氏名：" . $name ."\n";
	$body .= "メールアドレス：" . $email ."\n";	$body .= "住所：" . $address ."\n";
	$body .= "電話番号：" . $phone ."\n";
	$body .= "勤務先名：" . $company ."\n";
	$body .= "役職：" . $position ."\n";
	$body .= "カンボジア訪問の有無：" . $cambodia ."\n";	$body .= "投資予算：".$buget."\n";
	$body .= "海外不動産投資経験の有無：".$experience."\n";
	$body .= "問い合わせ内容：\n".$comment ."\n";	// 送信処理	mb_send_mail($to, $subject, $body, $header);
?>
<!DOCTYPE html><html lang="en"><head><title>お問い合わせ受付完了</title><meta charset="utf-8"><meta name="keywords" content="カンボジア,不動産,アンナキャムパートナーズ,内藤忍,不動産ツアー,不動産投資"><meta name="description" content="カンボジア不動産投資コラボレーション企画！期待の新物件 資産デザイン研究所先行ツアー開催決定！"><link rel="stylesheet" href="css/reset.css" type="text/css" media="screen"><link rel="stylesheet" href="css/style.css" type="text/css" media="screen"><link rel="stylesheet" href="css/grid.css" type="text/css" media="screen"><link rel="stylesheet" href="css/form.css" type="text/css" media="screen"><!--[if lt IE 9]><script type="text/javascript" src="js/html5.js"></script><![endif]--><!--<link rel="stylesheet" type="text/css" media="screen" href="js/jquery-validation-1.10.0/css/screen.css" /><script src="js/jquery-validation-1.10.0/lib/jquery-1.7.2.min.js"></script><script src="js/jquery-validation-1.10.0/dist/jquery.validate.min.js"></script><script src="js/jquery-validation-1.10.0/localization/messages_ja.js"></script><script type="text/javascript">$(function(){    $('#inquiry-form').validate();})</script>--><script>  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');  ga('create', 'UA-51807490-1', 'investment-cambodia.asia');  ga('send', 'pageview');</script></head><body id="page1"><div class="extra">  <!--==============================header=================================-->  <header>    <div class="main">      <div class="border-top">        <div class="wrapper">          <h1>お問い合わせありがとうございます。</h1>        </div><!-- wrapper -->      </div><!-- border-top -->      	<div class="indent-top"></div>    </div> <!-- main -->  </header>  <!--==============================content================================-->  <section id="content">    <div class="main"> 	<aside>		<div class="container_12">			<div class="wrapper p3">				<article class="grid_12">					<div class="text-center">					<h2>お問い合わせ内容を受付ました。回答まで少々お待ちください。</h2>					</div>					<div class="indent-top">					<div class="text-center">					<a href="index.html">ページに戻る</a>					</div>					</div>									</article><!-- grid_12 -->			</div><!-- wrapper p3 -->		</div><!-- container_12 -->	</aside>            </div>    </div>    <div class="block"></div>  </section><!--==============================footer=================================--><footer>  <div class="main">    <div class="footer-bg">      <div class="aligncenter">Copyright &copy; AnnaCam Partners Co.,Ltd All Rights Reserved<br></div>    </div>  </div></footer></body></html>
</div>
</body>
</html>
