<section id="content">
    <div class="full-width-container block-4">
        <div class="container">
            <div class="row">
                <div class="grid_12">
                    <header>
                        <h2>カンボジア投資本のご案内</h2>
                        <p class="sub_head"></p>
                    </header>
                </div>
                <div class="grid_2">
                    <p class="sub_head"></p>
                    <a href="http://urx2.nu/h5Sh" target="_blank"><img src="images/investment.jpg" alt="カンボジア書籍"></a><br/>
                    <center><a href="http://urx2.nu/h5Sh" target="_blank">http://urx2.nu/h5Sh</a></center>
                </div>
                <div class="grid_10">
                    <div class="box">
                        <p>東南アジア投資のラストリゾート　カンボジア</p>
                        <p>(黄金律新書) 新書 – 2015/1/24</p>
                        <p>カンボジアンナ（荒木杏奈） (著)</p>
                        <p>
                            内容紹介<br/>
                            タイ、マレーシアはもう手遅れ。南米、アフリカはまだ怖い。<br/>
                            快適に投資ができて、大きな成長が狙える国、それがカンボジアだ！
                        </p>
                        <p>
                            ●カンボジア不動産投資が狙い目といえる７つの理由<br/>
                            魅力その①　米ドル経済という大きなメリット<br/>
                            魅力その②　流れ込む海外マネー<br/>
                            魅力その③　タイトな供給状況<br/>
                            魅力その④　高利回りが見込める<br/>
                            魅力その⑤　東南アジア不動産と比べて割安<br/>
                            魅力その⑥　インカムゲイン＆キャピタルゲインが狙える<br/>
                            魅力その⑦　安心な不動産投資が可能な環境
                        </p>
                        <p>カンボジア経済は2010年度以降、リーマンショックから一気に息を吹き返し、他の東南アジア諸国と比較しても目覚ましい成長路線をたどっています。</p>
                        <p>
                            カンボジアほどさらなる成長余力のある国は他にありません。<br/>
                            なにより投資対象としてのカンボジアは、まだほとんどの人が手を付けていない、いわばラストリゾートなのです。<br/>
                            本書では、今なぜカンボジア投資が狙い目なのかを踏まえたうえで、その投資の魅力を金融・経済事情、不動産投資の順に解説していきます。
                        </p>
                        <p>カンボジアで実際に不動産を購入する際のシミュレーションや、投資を成功させるために欠かせないポイント、現地の投資物件の具体例についても紹介します。</p>
                    </div>
                </div>
                <div class="grid_12">
                    <p class="sub_head"></p>
                    <center>
                    <p>物件に関する詳しい情報に関して</p>
                    <p>今回ご紹介する物件及び、カンボジアプノンペンの不動産に関してはアンナキャムパートナーズまで直接お問い合わせください。</p>
                    </center>
                </div>
            </div>
		<p class="sub_head"></p>
		<center>
		<p class="sub_head"></p>
		<div class="row">
                    <center>
                        <div class="grid_4 center">
                            <a href="contact.php">
                                <img src="images/botton01.png" alt="contact">                                                    
                            </a>
                        </div>
                    </center>
		</div>
		<p class="sub_head"></p>
        </div>
</section>