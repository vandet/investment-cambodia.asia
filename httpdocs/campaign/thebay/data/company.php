<section id="content">		
    <div class="full-width-container block-4">
        <div class="container">
            <div class="row">
                <div class="grid_12">
                    <header>
                        <h2>会社概要</h2>
                        <p class="sub_head"></p>
                    </header>
                </div>
                <div class="grid_3">
                    <img src="images/logo_annacam.jpg" alt="アンナキャムパートナーズ"><br/><br/>
                    <img src="images/anna.jpg" alt="アンナ">
                </div>
                <div class="grid_9">
                	<p>【企業情報】</p>
                    <p>・会社名</p>
                    <p>アンナキャムパートナーズ株式会社</p>
                    <p>AnnaCam Partners Co., Ltd.</p>
                    <br/>
                    <p>・住所</p>
                    <p>SunCity No.255, St.51, corner of St. 370, Sangkat BKK1, Khan Chamkamorn, Phnom Penh, Cambodia</p><br/>
                    <p>・ホームページ</p>
                    <p><a target="_blank" href="http://investment-cambodia.asia">http://investment-cambodia.asia</a></p>
                    <p>・カンボジア不動産情報サイト 「ANNA HOME」
                    <p><a target="_blank" href="http://annahome.asia">http://annahome.asia</a></p>
                    <p>・カンボジアブログ</p>
                    <p><a target="_blank" href="http://cambodianna.blogspot.com">http://cambodianna.blogspot.com</a></p>
					<br/>
                    <p>E-mail <a href="mailto:info@annacampartners.com">info@annacampartners.com</a></p>
                    <p>Tel +855-12-833-290(直通) / +855-12-215-240  (竹口)</p>
                    <br/>
                    <p>代表取締役　荒木杏奈</p>
                    <p>1984年生まれ、東京都出身。大手ネット広告代理店（株）セプテーニ入社。その後SBIマーケティング（株）経て、2012年10月よりカンボジアの現地金融機関に勤務。2013年12月からアンナキャムパートナーズ株式会の代表取締役に就任。カンボジアには、自分自身でも投資をしており、2014年5月には3Bedのコンドミニアムを購入。</p>
                </div>
            </div>
		<p class="sub_head"></p>
		<center>
		<p class="sub_head"></p>
		<div class="row">
			<center>
                            <div class="grid_4 center">
                                <a href="contact.php">
                                    <img src="images/botton01.png" alt="contact">                                                    
                                </a>
                            </div>
                        </center>
		<p class="sub_head"></p>
		</div>
		<p class="sub_head"></p>
        </div>
</section>
