<script>
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>

<script>
!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
<script src="https://apis.google.com/js/platform.js" async defer></script>

<section id="content">
            <div class="full-width-container block-3">
                <div class="container">
                    <div class="row">
                        <div class="grid_12">
                            <div class="social_media">
                                <a class="fb-like" expr:data-href="data:post.url" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></a>
                               <label>&nbsp;&nbsp;</label>
                               <a href="https://twitter.com/share" class="twitter-share-button" data-count="none">ツイート</a>
                               &nbsp;
                                <div class="g-plusone" data-size="medium" data-annotation="none"></div>                                        </div>
                        </div>
                    </div>
                   <div class="row">
                        <div class="grid_12">
                            <header>
                                <h2>カンボジア最大級の不動産開発プロジェクト 「THE BAY」</h2>
                            </header>                        </div>
                   </div>
                    <div class="row">
                        <div class="grid_12">
                            <div class="box_noline">
                                <p>TEHO-SBG Development Co.,Ltd. (シンガポール上場企業TEHO International の子会社)による開発プロジェクト。<br/>
                                  場所は、プノンペン市のチョロイチャンバー半島で、東西にメコン川とトンレサップ川を臨む新興開発エリア。住宅、ホテル、商業施設等の一体型の複合不動産開発。<br/>
                                  最大53階建ての合計７棟の複合施設（住居６棟、ホテル1棟、低層階はいずれも商業施設）です。<br/>
                                  スタイリッシュなデザインは、シンガポールの著名な設計事務所「ONG&ONG」によるもので、2015年のアジアパシフィックプロパティーアワードを受賞済。<br/>
                                  さらに完成時から3年間は、家賃利回６％を保証（グロス）。
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <center>
                            <div class="grid_4 center">
                                <a href="contact.php">
                                    <img src="images/botton01.png" alt="contact">
                                </a>
                            </div>
                        </center>
                    </div>
                </div><!-- end of container -->
            </div><!-- end of blcok-3 -->

            <div class="full-width-container block-4">
                <div class="container">
				<div class="row">
					<div class="grid_12">
						<h2>プロジェクト概要</h2>
					</div>
				</div>
				<div class="box">
				<table width='100%' border='0' cellspacing='0' cellpadding='0' class='tbl_list_bg'>
					<tr>
						<td class='td_name' valign='top' width='20%'>開発会社</td>
						<td>TEHO-SBG Development Co.,Ltd.<br/>(シンガポール上場企業TEHO International の子会社)
						</td>
                                        </tr>
					<tr>
						<td class='td_name' valign='top' width='20%'>エリア</td>
						<td>プノンペン市チョロイチャンバー半島</td>
					</tr>
					<tr>
						<td class='td_name' valign='top' width='20%'>開発概要</td>
						<td>
						最大53階建ての合計7棟の複合施設（住居6棟、ホテル1棟、低層階はいずれも商業施設）、土地面積2万㎡<br/><br/>
						ベイスィート（1棟）：全室1ルームの住居棟<br>
						ベイレジデンス（4棟）：1〜3ベットルームの住居棟<br/>
						スカイヴィラ（1棟）：全室ペントハウス使用の住居棟<br/>
						ホテル棟（１棟）：250室のホテル(予定)<br/>
						</td>
					</tr>
					<tr>
						<td class='td_name' valign='top' width='20%'>販売形態</td>
						<td>住居棟コンドミニアムは、完全所有権</td>
					</tr>
					<tr>
						<td class='td_name' valign='top' width='20%'>プロジェクトの設計</td>
						<td>ONG&ONG<br/>(本プロジェクトのデザインは、既に2015年のアジアパシフィックプロパティーアワードを受賞)</td>
					</tr>
					<tr>
						<td class='td_name' valign='top' width='20%'>完成時期</td>
						<td>2019年9月末（予定：ベイスィート棟）</td>
					</tr>
					<tr>
						<td class='td_name' valign='top' width='20%'>特徴</td>
						<td>開発が進むチョロイチャンバー半島における53階建てのプノンペン最大級の開発プロジェクト。</td>
					</tr>
				</table>
				</div><!-- end of box for table -->
                                <p class="sub_head"></p>
                                <div class="row">
                                    <center>
                                        <div class="grid_4 center">
                                            <a href="contact.php">
                                                <img src="images/botton01.png" alt="contact">
                                            </a>
                                        </div>
                                    </center>
                                </div>
			</div><!-- end of container -->
		</div><!-- end of block-4 for project summary -->

            <div class="full-width-container block-5">
                    <div class="container">
                            <div class="row">
                                    <div class="grid_12">
                                            <h2>物件イメージビデオとロケーション</h2>
                                    </div>
                            </div>
                            <div class="row">
                                    <div class="grid_6">
                                        <iframe class="videos"  width="100%" height="315" src="https://www.youtube.com/embed/ZvxFDMHVRaU" frameborder="0" allowfullscreen></iframe>

                                            <p>物件イメージビデオ</p>
                                    </div>
                                    <div class="grid_6">
                                            <img src='images/map.png' alt='Image'>
                                            <p>ロケーション</p>
                                    </div>
                            </div>
                    </div>
            </div><!-- end of block-5 -->

            <div class="full-width-container block-4">
                    <div class="container">
                            <div class="row">
                                    <div class="grid_12">
                                            <h2>スカイガーデンからの眺望</h2>
                        <center>
                            <img src='images/panaromic_04.jpg' alt='Image'>
                        </center>
                        <p class="sub_head"></p>
                        <center>
                            <div class="grid_9 center">
                                <img src='images/top_1.png' alt='Image'>
                            </div>
                        </center>
                                    </div>
                            </div>
                            <div class="row">
                                    <div class="grid_12">
                                            <h2>第一期販売：ベイスイート棟について</h2>
                                            <center>
                                            <img src='images/baysuite.png' alt='Image'>
                                            </center>
                                    </div>
                            </div>
                        <br/>
                        <br/>
                            <div class="row">
                                <div class="grid_12">
                                    <div cla="box">
                                        <table width='100%' border='0' cellspacing='0' cellpadding='0' class='tbl_list_bg'>
                                            <tr>
                                                    <td class='td_name' valign='top' width='20%'>第一期 販売内容</td>
                                                    <td>ベイスィート棟 688戸</td>
                                            </tr>
                                            <tr>
                                                    <td class='td_name' valign='top'>建物概要</td>
                                                    <td>プノンペン市チョロイチャンバー半島</td>
                                            </tr>
                                            <tr>
                                                    <td class='td_name' valign='top'>開発概要</td>
                                                    <td>
                                                        1〜５階駐車場、6・7階共有施設、<br/>
                                                        8〜52階が住居、53階はスカイガーデン<br/><br/>
                                                        プロジェクトの南西に位置し、南向きの部屋からはメコン河（東）とトンレサップ川（西）、北向きの部屋からは、メコン河（東）の景観。

                                                    </td>
                                            </tr>
                                            <tr>
                                                    <td class='td_name' valign='top'>販売価格</td>
                                                    <td>150,500〜243,200USD（平均価格200,892USD）</td>
                                            </tr>
                                            <tr>
                                                    <td class='td_name' valign='top'>間取り</td>
                                                    <td>
                                                        全室１ルーム、39.9〜47.14㎡<br/>
                                                        天井高：2.7m（バスルーム2.4m）、フロア間3.2m
                                                    </td>
                                            </tr>
                                            <tr>
                                                    <td class='td_name' valign='top'>完成時期</td>
                                                    <td>2019年9月末</td>
                                            </tr>
                                            <tr>
                                                    <td class='td_name' valign='top'>賃貸 利回り保証</td>
                                                    <td>
                                                        2019年10月（完成予定）からの３年間は、購入価格の6%の年間家賃利回りを保証！<br/>
                                                        さらに4年目以降も延長の可能性あり。<br/>
                                                        グロス利回、税・諸経費控除後のネット利回りは4％後半
                                                    </td>
                                            </tr>
                                            <tr>
                                                    <td class='td_name' valign='top'>支払方法</td>
                                                    <td>予約時1,000USD ＋ 仲介料　※詳しくはお問い合わせください</td>
                                            </tr>
                                            <tr>
                                                <td class='td_name' rowspan="3" valign='middle' aling='center'>お支払方法</td>
                                                    <td>契約時：物件価格の20％</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    2016年2月：物件価格の10％<br/>
                                                    2016年8月：物件価格の10％<br/>
                                                    2017年2月：物件価格の10％<br/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    完成時：物件価格の50％<br/><br/>
                                                    2018年の支払はゼロ
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class='td_name' valign='top'>ベイスィート棟 専用施設</td>
                                                <td>
                                                    コンシェルジュサービス<br/>
                                                    高級ジム、25mラッププール、大人用ラウンジループ、子供専用プール<br/>
                                                    BBQ広場、７つの多目的ルーム（会議用）<br/>
                                                    スカイテラス＆ラウンジ30階<br/>
                                                    ルーフトップガーデン53階
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class='td_name' valign='top'>全体共有施設</td>
                                                <td>
                                                    インフィニティ形式の25mラッププール、<br/>
                                                    バブル＆ジェットプール<br/>
                                                    ジャグジー＆アクアジム<br/>
                                                    Spa＆サウナ<br/>
                                                    Audio & Video ルーム、エンターティメントルーム<br/>
                                                    スカイジム、スカイテラスデッキ、展望ラウンジ
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class='td_name' valign='middle'>間取り例</td>
                                                <td>
                                                    <img src="images/floorplan.png" alt="間取り例"/>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <p class="sub_head"></p>
                            <div class="row">
                                <center>
                                    <div class="grid_4 center">
                                        <a href="contact.php">
                                            <img src="images/botton01.png" alt="contact">
                                        </a>
                                    </div>
                                </center>
                            </div>
                    </div>
            </div>

		<div class="full-width-container block-5 stellar-block" data-stellar-background-ratio="0.1">
			<div class="container">
				<div class="row">
					<div class="grid_12">
						<h2>イメージ画像</h2>
						<p class="sub_head">スカイガーデン、エントランス、お部屋</p>
					</div>
				</div>
				<div class="row" data-gallery="hongkong" id="touch_gallery">
					<div class="grid_3">
						<a href="images/sky_lounge.png"><img src="images/sky_lounge_thum.png" alt="スカイガーデン"><div></div></a>
						<h4>スカイガーデン</h4>
					</div>
					<div class="grid_3">
						<a href="images/entrance.png"><img src="images/entrance_thum.png" alt="エントランス"><div></div></a>
						<h4>エントランス</h4>
					</div>
					<div class="grid_3">
						<a href="images/bedroom.png"><img src="images/bedroom_thum.png" alt="ベッドルーム"><div></div></a>
						<h4>お部屋1</h4>
					</div>
					<div class="grid_3">
						<a href="images/kitchen_bedroom.png"><img src="images/kitchen_bedroom_thum.png" alt="キッチンとベッドルーム"><div></div></a>
						<h4>お部屋2</h4>
					</div>
				</div>
			</div>
		</div>

	</section>
