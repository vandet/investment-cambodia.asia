<link rel="stylesheet" href="css/contact-form.css">
<script src="js/TMForm.js"></script>
<script src="js/modal.js"></script>      
<section id="content">    
    <div class="full-width-container block-4">
        <div class="container">
            <div class="row">
                <div class="grid_12">
                    <h2 class="contact_form">視察ツアーのお申込み</h2>
                </div>
            </div>
            <div class="row">
                <form id="contact-form" method="post" name="contact-form">
                    <div class="contact-form-loader"></div>
                        <fieldset>
                            <div class="grid_6">
                                ツアー日程：
                                <label>
                                    <select name="tour_date" data-constraints="@Required" >
                                        <option value="">ツアー日程：</option>
                                        <option vlaue="7月03日（金）〜 7日06日（月）">⑴　7月03日（金）〜 7日06日（月）</option>
                                        <option vlaue="7月17日（金）〜 7日20日（月）">⑵　7月17日（金）〜 7日20日（月）</option>
                                        <option vlaue="7月24日（金）〜 7日27日（月）">⑶　7月24日（金）〜 7日27日（月）</option>
                                        <option vlaue="8月07日（金）〜 8日10日（月）">⑷　8月07日（金）〜 8日10日（月）</option>
                                    </select>
                                    
                                    <span class="empty-message">*This field is required.</span>
                                    <span class="error-message">*This is not a valid date.</span>
                                </label>
                                <label class="name">
                                    <input type="text" name="name" placeholder="お名前:" value="" data-constraints="@Required" />
                                    <span class="empty-message">*This field is required.</span>
                                    <span class="error-message">*This is not a valid name.</span>
                                </label>
                                <label class="company">
                                    <input type="text" name="company" placeholder="会社名:" value="" data-constraints="@Required" />
                                    <span class="empty-message">*This field is required.</span>
                                    <span class="error-message">*This is not a valid company.</span>
                                </label>
                                <label class="position">
                                    <input type="text" name="position" placeholder="役職:" value="" data-constraints="@Required" />
                                    <span class="empty-message">*This field is required.</span>
                                    <span class="error-message">*This is not a valid position.</span>
                                </label>
                                <label class="phone">
                                    <input type="text" name="phone" placeholder="電話番号:" value="" data-constraints="@Required @JustNumbers" />
                                    <span class="empty-message">*This field is required.</span>
                                    <span class="error-message">*This is not a valid phone.</span>
                                </label>
                                <label class="email">
                                    <input type="text" name="email" placeholder="E-mail:" value="" data-constraints="@Required @Email" />
                                    <span class="empty-message">*This field is required.</span>
                                    <span class="error-message">*This is not a valid email.</span>
                                </label>
                                <br/>
                                <br/>
                                <label class="travel">
                                    カンボジア渡航経験： &nbsp;
                                    <input type="radio" name="travel" id="travel_yes" checked="checked" value="有" />有 &nbsp;
                                </label>
                                <label class="travel">
                                    <input type="radio" name="travel" id="travel_no" value="無" />無                                  
                                </label>
                                <br/>
                                <br/>
                                <label class="sallary">
                                    <input type="text" name="sallary" placeholder="ご予算:" value="" data-constraints="@Required" />
                                    <span class="empty-message">*This field is required.</span>
                                    <span class="error-message">*This is not a valid sallary.</span>
                                </label>
                                <label class="real_estate">
                                    不動産投資経験： &nbsp;
                                    <input type="radio" name="real_estate" checked="checked" value="有" />有 &nbsp;
                                </label>
                                <label class="real_estate">
                                    <input type="radio" name="real_estate" value="無" />無 
                                </label>
                                <br/>
                                <br/>
                            </div>
                            
                            <div class="grid_6">
                                <label class="message">
                                    <textarea name="message" placeholder="お問い合わせ内容" data-constraints='@Required @Length(min=20,max=999999)'></textarea>
                                    <span class="empty-message">*This field is required.</span>
                                    <span class="error-message">*The message is too short.</span>
                                </label>
                            </div>
                            <input type='hidden' name='msg_alert' value='tour' >
                                <!-- <label class="recaptcha"><span class="empty-message">*This field is required.</span></label> -->
                        <div class="grid_12">
                            <a href="#" data-type="submit" class="btn">送信</a>
                        </div>
                    </fieldset> 
                    <div class="modal fade response-message">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">お問い合わせありがとうございます。</h4>
                                </div>
                                <div class="modal-body">
                                    お問い合わせを受け付けましたので、ご連絡まで少々お待ちください。
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>