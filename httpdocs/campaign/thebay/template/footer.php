<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="grid_2">
                <div class="logo">
					<img src="images/annacam_logo.png" style="width:130px">
                </div>
            </div>
            <div class="grid_10">
                <nav>
                    <ul class="footer-menu clearfix">
                        <li <?= $current_page=='contact'?'class="current"':'';?>><a href="contact.php">お問い合わせ</a></li>
                        <li <?= $current_page=='book'?'class="current"':'';?>><a href="book.php">書籍</a></li>
                        <li <?= $current_page=='company'?'class="current"':'';?>><a href="company.php">企業情報</a></li>
                       <!-- <li <?= $current_page=='tour'?'class="current"':'';?>><a href="tour.php">視察ツアーのご案内</a></li> -->
                        <li <?= $current_page=='cambodia'?'class="current"':'';?>><a href="cambodia.php">なぜカンボジア？</a></li>
                        <li <?= $current_page=='index'?'class="current"':'';?>><a href="index.php">ホーム</a></li>
                    </ul>
                </nav>
                <!--
                <div class="social">
                    <a href="#">Google+</a>
                    <a href="#">Skype</a>
                    <a href="#">twitter</a>
                    <a href="#">facebook</a>
                </div>
                -->
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="grid_12 copyright">
                <pre>© <span id="copyright-year"></span>　AnnaCam Partners Co.,Ltd    <!--|   
                    <a href="index-6.html">Privacy Policy</a>-->
                </pre>
                            <!--{%FOOTER_LINK} -->
            </div>
        </div>
    </div>
</footer>