<header id="header">
    <div id="stuck_container">
        <div class="full-width-container block-1">
            <div class="container">
                <div class="row">
                    <div class="logo">
                    	<!--
                        <h1>
                            <a href="index.html">
                                ANNACAM
                                <span>PARTNERS CO., LTD</span>
                            </a>
                        </h1>
                        -->
                        <h2>THE BAY</h2>
                        <h3>THE HOTSPOT OF HIGH POTENTIAL INVESTMENTS</h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="full-width-container block-2">
            <div class="container">
                <div class="row">
                    <div class="grid_12">
                        <nav>
                            <ul class="sf-menu">
                                <li <?= $current_page=='index'?'class="current"':'';?>><a href="index.php">ホーム</a></li>
                                <li <?= $current_page=='cambodia'?'class="current"':'';?>><a href="cambodia.php">なぜカンボジア？</a></li>
                                <!-- <li <?= $current_page=='tour'?'class="current"':'';?>><a href="tour.php">視察ツアーのご案内</a></li> -->
                                <li <?= $current_page=='company'?'class="current"':'';?>><a href="company.php">企業情報</a></li>
                                <li <?= $current_page=='book'?'class="current"':'';?>><a href="book.php">書籍</a></li>
                                <li <?= $current_page=='contact'?'class="current"':'';?>><a href="contact.php">お問い合わせ</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
