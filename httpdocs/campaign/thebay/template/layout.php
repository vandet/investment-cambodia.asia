<?php
$class_bordy = $class_bordy!=''?$class_bordy:'';
$page_title=$page_title !=''?$page_title:'【THE BAY】急成長国カンボジアの最大級の不動産開発プロジェクト';
$current_page =$current_page !=''?$current_page:'【THE BAY】急成長国カンボジアの最大級の不動産開発プロジェクト';
$page_keyword =$page_keyword !=''?$page_keyword:'THE BAY, カンボジア不動産,不動産,カンボジア,アンナキャムパートナーズ,annacam partners';
$page_description =$page_description !=''?$page_description:'【THE BAY】急成長国カンボジアの最大級の不動産開発プロジェクト。カンボジア不動産のことならアンナキャムパートナーズへ。';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    
		<title><?= $page_title?></title>
                <meta name="keywords" content="<?= $page_keyword?>"/>
                <meta name="description" content="<?= $page_description?>"/>
		<meta charset="utf-8">
		<meta name = "format-detection" content = "telephone=no" />
		<link rel="icon" href="images/favicon.ico" type="image/x-icon">
		<link rel="stylesheet" href="css/grid.css">
		<link rel="stylesheet" href="css/style.css">
		<link rel="stylesheet" href="css/touchTouch.css">
		<script src="js/jquery.js"></script>
		<script src="js/jquery-migrate-1.2.1.js"></script>
		<script src="js/touchTouch.jquery.js"></script>
		<!--[if lt IE 8]>
		<div id="ie6-alert" style="width: 100%; text-align:center;">
				<img src="http://beatie6.frontcube.com/images/ie6.jpg" alt="Upgrade IE 6" width="640" height="344" border="0" usemap="#Map" longdesc="http://die6.frontcube.com" />
						<map name="Map" id="Map"><area shape="rect" coords="496,201,604,329" href="http://www.microsoft.com/windows/internet-explorer/default.aspx" target="_blank" alt="Download Interent Explorer" /><area shape="rect" coords="380,201,488,329" href="http://www.apple.com/safari/download/" target="_blank" alt="Download Apple Safari" /><area shape="rect" coords="268,202,376,330" href="http://www.opera.com/download/" target="_blank" alt="Download Opera" /><area shape="rect" coords="155,202,263,330" href="http://www.mozilla.com/" target="_blank" alt="Download Firefox" />
								<area shape="rect" coords="35,201,143,329" href="http://www.google.com/chrome" target="_blank" alt="Download Google Chrome" />
						</map>
		</div>
		<![endif]-->
		<!--[if lt IE 9]>
				<script src="js/html5shiv.js"></script>
		<link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
		<![endif]-->
</head>
<body <?= $class_bordy;?>>
<!--===============HEADER ==============-->
    <?php include('header.php')?>
<!--===============CONTENT ==============-->
    <?php include($content_data);?>

    <!--========== FOOTER ============-->
    <?php include('footer.php')?>
<script src="js/script.js"></script>
<script>
    $(function(){
        $('#touch_gallery a').touchTouch();
    });
</script>
</body>
</html>