

//font size
var domain = "/test/common/css/"

$(document).ready(function(){
	document.getElementById("sizeChange").href = domain + $.cookie('stylefont');
});

function setCss(cssurl){
	document.getElementById("sizeChange").href = domain + cssurl;
	$.cookie('stylefont',cssurl,{expires:60,path:'/'});
}

function main(){
	history.back();
}

$(document).ready(function(){
	// TOP IMG
	$("#topImg").find("ul").find("li:last").fadeIn("slow",function(){
		$("#topImg").find("ul").find("li").css("display","block");
	});

	function change(){
		$("#topImg").find("ul").find("li:last").fadeTo("slow",0,function(){
			$("#topImg").find("ul").find("li:last").prependTo("#topImg ul");
			$("#topImg").find("ul").find("li:first").css("opacity","1");
		});
	};

	var timerID;
	init();
	function init(){
		timerID = setInterval(function(){
			change();
		},4000);
	};
	
	
	// GNAV
	$("#gNav").find("ul").find("li").hover(function(){
		$("> div:not(:animated)",this).slideDown(300);
		$("> a",this).addClass("on");
	},function(){
		$("> div",this).slideUp(100);
		$("> a",this).removeClass("on");
	});
	
	
	// ROLLOVER
	$(".rollover").hover(
		function () {
			$(this).css("opacity","0.6");
		},
		function () {
			$(this).css("opacity","1");
		}
	);
	
	
	// faq

	$("#faqList").find("dt").toggle(function(){
		$("+dd",$(this)).slideDown("fast");
	},function(){
		$("+dd",$(this)).slideUp("fast");
	});
	$("#faqList").find("dt.open").toggle(function(){
		$("+dd",$(this)).slideUp("fast");
	},function(){
		$("+dd",$(this)).slideDown("fast");
	});
	$("#faqList").find("dt").mouseover(function(){
		$(this).addClass("hover");
	});
	$("#faqList").find("dt").mouseout(function(){
		$(this).removeClass("hover");
	});
	
});