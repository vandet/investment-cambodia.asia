#!/usr/bin/php
<?php

/**
 * activate jetpack plugin.
 */

$basepath = dirname(__FILE__);
require_once $basepath . '/enable_jetpack_admin.php';

$plugin = 'jetpack/jetpack.php';
$result = activate_plugin($plugin, '', false);

if($result != null) {
  trigger_error('Unable to enable jetpack plugin.', E_USER_ERROR);
}




