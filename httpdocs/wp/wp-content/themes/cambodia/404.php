<!DOCTYPE HTML>
<html>
<?php get_header(); ?>
	<body class="other">
<?php include (TEMPLATEPATH . '/head.php'); ?>
		<div id="pageTitle">
			<h2 style="color:#fff;"><?php _e('Error 404 - Not Found', ''); ?></h2>
		</div>
		<div id="towColumn">
			<div id="leftColumn">
			</div>
			<div id="rightColumn">
				<aside>
					<section class="contactBox contactUnder clearfix">
						<p><img src="<?php bloginfo( 'url' ); ?>/common/img/right_contact.gif" alt="お問い合わせ +855 12 215 240 土日祝日を除く9:00～18:00" width="250" height="145"></p>
						<p class="mail"><a href="<?php bloginfo( 'url' ); ?>/contact/"><img src="<?php bloginfo( 'url' ); ?>/common/img/right_contact_mail.jpg" alt="お問い合わせフォーム" width="230" height="45" class="rollover"></a></p>
					</section>
				</aside>
			</div>
		</div>
<?php get_footer(); ?>
<?php include (TEMPLATEPATH . '/google-code.php'); ?>
	</body>
</html>
