<!DOCTYPE HTML>
<html>
<?php get_header(); ?>
	<body class="other">
<?php include (TEMPLATEPATH . '/head.php'); ?>
		<div id="pageTitle">
			<h2><img src="<?php bloginfo( 'url' ); ?>/common/img/pagetitle01.png" alt="What's New News" width="150" height="40"></h2>
		</div>
		<div id="towColumn">
			<div id="leftColumn">
				<article class="newsList">
					<dl>
<?php query_posts($query_string . '&' .'&orderby=date');
if (have_posts()) : while (have_posts()) : the_post(); ?>
						<dt><?php echo get_the_date('Y-m-d'); ?></dt>
						<dd><a href="<?php the_permalink(); ?>"><?php if(mb_strlen( $post->post_title ) < 50) { echo $post->post_title; } else { echo mb_substr($post->post_title, 0, 50).'…';} ?></a></dd>
<?php endwhile; endif; wp_reset_query(); ?>
					</dl>
				</article>
<?php if (function_exists("pagination")) {
	pagination($additional_loop->max_num_pages);
} ?>
			</div>
			<div id="rightColumn">
				<nav>
					<dl>
						<dt><span><img src="<?php bloginfo( 'url' ); ?>/common/img/right_title01.png" alt="News" width="250" height="30"></span></dt>
						<dd class="top"><a href="<?php bloginfo( 'url' ); ?>/news/">What's New</a></dd>
					</dl>
				</nav>
				<aside>
					<section class="banner">
						<ul>
							<li><a href="http://annahome.asia" target="_blank"><img src="<?php bloginfo( 'url' ); ?>/common/img/right_banner01.png" alt="Anna Home" width="250" height="90" class="rollover"></a></li>
							<li><a href="http://cambodianna.blogspot.com/" target="_blank"><img src="<?php bloginfo( 'url' ); ?>/common/img/right_banner02.png" alt="ジャパンデスクアンナのカンボジア投資情報" width="250" height="90" class="rollover"></a></li>
						</ul>
					</section>
					<section class="contactBox contactUnder clearfix">
						<p><img src="<?php bloginfo( 'url' ); ?>/common/img/right_contact.gif" alt="お問い合わせ +855 12 215 240 土日祝日を除く9:00～18:00" width="250" height="145"></p>
						<p class="mail"><a href="<?php bloginfo( 'url' ); ?>/contact/"><img src="<?php bloginfo( 'url' ); ?>/common/img/right_contact_mail.jpg" alt="お問い合わせフォーム" width="230" height="45" class="rollover"></a></p>
					</section>
				</aside>
			</div>
		</div>
<?php get_footer(); ?>
<?php include (TEMPLATEPATH . '/google-code.php'); ?>
	</body>
</html>
