<?php include (TEMPLATEPATH . '/parts-page-top.php'); ?>
<section id="link01" class="contactConfirm">
	<h3><img src="<?php bloginfo( 'url' ); ?>/common/img/title_middle27.gif" alt="お問い合わせ" width="700" height="30"></h3>
	<p class="requiredMark">必須項目</p>
	<form action="#link01" method="post">
		<input type="hidden" name="mode" value="do_form2">
		<input type="hidden" name="nameA" value="<?=$_POST['nameA']?>">
		<input type="hidden" name="nameB" value="<?=$_POST['nameB']?>">
		<input type="hidden" name="com" value="<?=$_POST['com']?>">
		<input type="hidden" name="post" value="<?=$_POST['post']?>">
		<input type="hidden" name="unit" value="<?=$_POST['unit']?>">
		<input type="hidden" name="telCom" value="<?=$_POST['telCom']?>">
		<input type="hidden" name="telMobile" value="<?=$_POST['telMobile']?>">
		<input type="hidden" name="mailA" value="<?=$_POST['mailA']?>">
		<input type="hidden" name="mailB" value="<?=$_POST['mailB']?>">
		<input type="hidden" name="means" value="<?=$_POST['means']?>">
		<input type="hidden" name="cts" value="<?=$_POST['cts']?>">
		<dl>
			<dt class="required">氏名（漢字）</dt>
			<dd><?=$_POST['nameA']?></dd>
		</dl>
		<dl>
			<dt class="required">氏名（カタカナ）</dt>
			<dd><?=$_POST['nameB']?></dd>
		</dl>
		<dl>
			<dt class="required">会社名</dt>
			<dd><?=$_POST['com']?></dd>
		</dl>
		<dl>
			<dt>役職</dt>
			<dd><?=$_POST['post']?></dd>
		</dl>
		<dl>
			<dt class="required">部署名</dt>
			<dd><?=$_POST['unit']?></dd>
		</dl>
		<dl>
			<dt class="required">電話番号（会社）</dt>
			<dd><?=$_POST['telCom']?></dd>
		</dl>
		<dl>
			<dt class="required">電話番号（携帯）</dt>
			<dd><?=$_POST['telMobile']?></dd>
		</dl>
		<dl>
			<dt class="required">メールアドレス</dt>
			<dd><?=$_POST['mailA']?></dd>
		</dl>
		<dl>
			<dt class="required">メールアドレス（確認）</dt>
			<dd><?=$_POST['mailB']?></dd>
		</dl>
		<dl>
			<dt>ご希望のご連絡手段</dt>
			<dd><?=$_POST['means']?></dd>
		</dl>
		<dl>
			<dt class="required">お問い合わせ及び、ご希望のサービス内容の記載</dt>
			<dd><?=$_POST['cts']?></dd>
		</dl>
		<p class="submit"><input name="submit" type="submit" value="送信する" class="rollover"><input type="button" value="入力しなおす" class="rollover" onClick="main()" onKeyPress="main()"></p>
	</form>
</section>
<section id="link02" class="mailMagazine">
	<h3><img src="<?php bloginfo( 'url' ); ?>/common/img/title_middle28.gif" alt="メールマガジン登録" width="700" height="30"></h3>
	<div class="mailBox">
		<div>
			<h4><span>登録</span></h4>
			<form action="#link02" method="post">
				<input type="hidden" name="mode" value="do_form_m1">
				<dl>
					<dt>メールアドレス</dt>
					<dd><input name="mail01A" type="email" required></dd>
				</dl>
				<dl>
					<dt>メールアドレス確認用</dt>
					<dd><input name="mail01B" type="email" required></dd>
				</dl>
				<p class="submit"><input name="submit" type="submit" value="登録" class="rollover"></p>
			</form>
		</div>
	</div>
	<div class="mailBox">
		<div>
			<h4><span>解除</span></h4>
			<form action="#link02" method="post">
				<input type="hidden" name="mode" value="do_form_m2">
				<dl>
					<dt>メールアドレス</dt>
					<dd><input name="mail02A" type="email" required></dd>
				</dl>
				<dl>
					<dt>メールアドレス確認用</dt>
					<dd><input name="mail02B" type="email" required></dd>
				</dl>
				<dl>
					<dt>解除理由</dt>
					<dd><textarea name="cts02" cols="20" rows="5" required></textarea></dd>
				</dl>
				<p class="submit"><input name="submit" type="submit" value="解除" class="rollover"></p>
			</form>
		</div>
	</div>
</section>
<?php if (have_posts()) : while (have_posts()) : the_post();?>
<?php the_content(); ?>
<?php endwhile; endif; ?>
<?php include (TEMPLATEPATH . '/parts-page-btm.php'); ?>
