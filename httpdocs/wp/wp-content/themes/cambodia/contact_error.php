<?php include (TEMPLATEPATH . '/parts-page-top.php'); ?>
<section id="link01" class="contactError">
	<h3><img src="<?php bloginfo( 'url' ); ?>/common/img/title_middle27.gif" alt="お問い合わせ" width="700" height="30"></h3>
	<p>入力エラーがありました。お手数ですが、戻って再入力してください。</p>
	<p id="error"><?=$msg?></p>
	<p class="submit"><input type="button" value="戻る" onClick="main()" onKeyPress="main()" class="rollover"></p>
</section>
<section id="link02" class="mailMagazine">
	<h3><img src="<?php bloginfo( 'url' ); ?>/common/img/title_middle28.gif" alt="メールマガジン登録" width="700" height="30"></h3>
	<div class="mailBox">
		<div>
			<h4><span>登録</span></h4>
			<form action="#link02" method="post">
				<input type="hidden" name="mode" value="do_form_m1">
				<dl>
					<dt>メールアドレス</dt>
					<dd><input name="mail01A" type="email" required></dd>
				</dl>
				<dl>
					<dt>メールアドレス確認用</dt>
					<dd><input name="mail01B" type="email" required></dd>
				</dl>
				<p class="submit"><input name="submit" type="submit" value="登録" class="rollover"></p>
			</form>
		</div>
	</div>
	<div class="mailBox">
		<div>
			<h4><span>解除</span></h4>
			<form action="#link02" method="post">
				<input type="hidden" name="mode" value="do_form_m2">
				<dl>
					<dt>メールアドレス</dt>
					<dd><input name="mail02A" type="email" required></dd>
				</dl>
				<dl>
					<dt>メールアドレス確認用</dt>
					<dd><input name="mail02B" type="email" required></dd>
				</dl>
				<dl>
					<dt>解除理由</dt>
					<dd><textarea name="cts02" cols="20" rows="5" required></textarea></dd>
				</dl>
				<p class="submit"><input name="submit" type="submit" value="解除" class="rollover"></p>
			</form>
		</div>
	</div>
</section>
<?php if (have_posts()) : while (have_posts()) : the_post();?>
<?php the_content(); ?>
<?php endwhile; endif; ?>
<?php include (TEMPLATEPATH . '/parts-page-btm.php'); ?>
