<?php include (TEMPLATEPATH . '/parts-page-top.php'); ?>
<section id="link01" class="contact">
	<h3><img src="<?php bloginfo( 'url' ); ?>/common/img/title_middle27.gif" alt="お問い合わせ" width="700" height="30"></h3>
	<p class="requiredMark">必須項目</p>
	<form action="#link01" method="post">
		<input type="hidden" name="mode" value="do_form1">
		<dl>
			<dt class="required">氏名（漢字）</dt>
			<dd><input name="nameA" type="text" required></dd>
		</dl>
		<dl>
			<dt class="required">氏名（カタカナ）</dt>
			<dd><input name="nameB" type="text" required></dd>
		</dl>
		<dl>
			<dt class="required">会社名</dt>
			<dd><input name="com" type="text" required></dd>
		</dl>
		<dl>
			<dt>役職</dt>
			<dd><input name="post" type="text"></dd>
		</dl>
		<dl>
			<dt class="required">部署名</dt>
			<dd><input name="unit" type="text" required></dd>
		</dl>
		<dl>
			<dt class="required">電話番号（会社）</dt>
			<dd><input name="telCom" type="tel" required></dd>
		</dl>
		<dl>
			<dt class="required">電話番号（携帯）</dt>
			<dd><input name="telMobile" type="tel" required></dd>
		</dl>
		<dl>
			<dt class="required">メールアドレス</dt>
			<dd><input name="mailA" type="email" required></dd>
		</dl>
		<dl>
			<dt class="required">メールアドレス（確認）</dt>
			<dd><input name="mailB" type="email" required></dd>
		</dl>
		<dl>
			<dt>ご希望のご連絡手段</dt>
			<dd><input name="means" type="text"></dd>
		</dl>
		<dl>
			<dt class="required">お問い合わせ及び、ご希望のサービス内容の記載</dt>
			<dd><textarea name="cts" cols="20" rows="5" required></textarea></dd>
		</dl>
		<p class="submit"><input name="submit" type="submit" value="確認画面へ" class="rollover"></p>
	</form>
</section>
<section id="link02" class="mailMagazine">
	<h3><img src="<?php bloginfo( 'url' ); ?>/common/img/title_middle28.gif" alt="メールマガジン登録" width="700" height="30"></h3>
	<div class="mailBox">
		<div>
			<h4><span>登録</span></h4>
			<form action="#link02" method="post">
				<input type="hidden" name="mode" value="do_form_m1">
				<dl>
					<dt>メールアドレス</dt>
					<dd><input name="mail01A" type="email" required></dd>
				</dl>
				<dl>
					<dt>メールアドレス確認用</dt>
					<dd><input name="mail01B" type="email" required></dd>
				</dl>
				<p class="submit"><input name="submit" type="submit" value="登録" class="rollover"></p>
			</form>
		</div>
	</div>
	<div class="mailBox">
		<div>
			<h4><span>解除</span></h4>
			<p>入力エラーがありました。お手数ですが、戻って再入力してください。</p>
			<p id="error"><?=$msgM02?></p>
			<p class="submit"><input type="button" value="戻る" onClick="main()" onKeyPress="main()" class="rollover"></p>
		</div>
	</div>
</section>
<?php if (have_posts()) : while (have_posts()) : the_post();?>
<?php the_content(); ?>
<?php endwhile; endif; ?>
<?php include (TEMPLATEPATH . '/parts-page-btm.php'); ?>
