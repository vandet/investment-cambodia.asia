		<footer>
			<div id="topicPath">
				<div>
					<ul>
<?php if (is_home()) : ?>
						<li class="last">TOP</li>
<?php else : ?>
						<li><a href="<?php bloginfo( 'url' ); ?>/">TOP</a></li>
						<li>&gt;</li>
<?php if (is_single()) : ?>
						<li><a href="<?php bloginfo( 'url' ); ?>/news/">What's New</a></li>
						<li>&gt;</li>
<?php endif; ?>
						<li class="last"><?php wp_title('',true,'right'); ?></li>
<?php endif; ?>
					</ul>
					<p><a href="#header"><img src="<?php bloginfo( 'url' ); ?>/common/img/footer_pagetop.jpg" alt="PAGE TOP" width="80" height="30"></a></p>
				</div>
			</div>
			<div id="footerBack">
				<div class="subNav">
					<ul>
						<li><a href="<?php bloginfo( 'url' ); ?>/company/#link02">会社概要</a></li>
						<li><a href="<?php bloginfo( 'url' ); ?>/privacy/">プライバシーポリシー</a></li>
						<li><a href="<?php bloginfo( 'url' ); ?>/company/#link07">お問い合わせ後の流れ</a></li>
						<li><a href="<?php bloginfo( 'url' ); ?>/sitemap/">サイトマップ</a></li>
						<li><a href="<?php bloginfo( 'url' ); ?>/contact/">お問い合わせ</a></li>
					</ul>
				</div>
				<div id="fLink">
					<ul>
						<li class="fCts">
							<p class="logo"><img src="<?php bloginfo( 'url' ); ?>/common/img/footer_logo.jpg" alt="アンナキャムパートナーズ株式会社" width="80" height="34"></p>
							<p class="sub">カンボジア/コンサルティング</p>
							<p class="copy"><small>&copy; AnnaCam Partners Co.,Ltd</small></p>
						</li>
						<li class="box">
							<dl>
								<dt><a href="<?php bloginfo( 'url' ); ?>/company/">会社案内</a></dt>
								<dd><a href="<?php bloginfo( 'url' ); ?>/company/#link01">弊社のミッション</a></dd>
								<dd><a href="<?php bloginfo( 'url' ); ?>/company/#link02">会社概要</a></dd>
								<dd><a href="<?php bloginfo( 'url' ); ?>/company/#link03">メンバー紹介</a></dd>
								<dd><a href="<?php bloginfo( 'url' ); ?>/company/#link04">アクセスマップ</a></dd>
								<dd><a href="<?php bloginfo( 'url' ); ?>/company/#link07">お問い合わせ後の流れ</a></dd>
							</dl>
						</li>
						<li class="box">
							<dl>
								<dt><a href="<?php bloginfo( 'url' ); ?>/why/">なぜカンボジア</a></dt>
								<dd><a href="<?php bloginfo( 'url' ); ?>/why/#link01">概況</a></dd>
								<dd><a href="<?php bloginfo( 'url' ); ?>/why/#link02">進出メリット</a></dd>
								<dd><a href="<?php bloginfo( 'url' ); ?>/why/#link03">投資メリット</a></dd>
								<dd><a href="<?php bloginfo( 'url' ); ?>/why/#link04">アジア諸国との比較</a></dd>
								<dd><a href="<?php bloginfo( 'url' ); ?>/why/#link05">関連リンク集</a></dd>
							</dl>
						</li>
						<li class="box">
							<dl>
								<dt><a href="<?php bloginfo( 'url' ); ?>/investment/">投資/進出情報</a></dt>
								<dd><a href="<?php bloginfo( 'url' ); ?>/investment/#link01">カンボジア投資の魅力</a></dd>
								<dd><a href="<?php bloginfo( 'url' ); ?>/investment/#link02">金融市場情報</a></dd>
								<dd><a href="<?php bloginfo( 'url' ); ?>/investment/#link03">不動産情報</a></dd>
								<dd><a href="<?php bloginfo( 'url' ); ?>/investment/#link05">視察</a></dd>
							</dl>
						</li>
						<li class="box">
							<dl>
								<dt><a href="<?php bloginfo( 'url' ); ?>/seminar/">セミナー情報</a></dt>
								<dd><a href="<?php bloginfo( 'url' ); ?>/seminar/#link01">不動産投資セミナー</a></dd>
								<dd><a href="<?php bloginfo( 'url' ); ?>/seminar/#link02">視察</a></dd>
							</dl>
						</li>
						<li class="box boxRight">
							<dl>
								<dt><a href="<?php bloginfo( 'url' ); ?>/contact/">お問い合わせ</a></dt>
								<dd><a href="<?php bloginfo( 'url' ); ?>/contact/#link01">お問い合わせ</a></dd>
								<dd><a href="<?php bloginfo( 'url' ); ?>/contact/#link02">メールマガジン登録</a></dd>
								<dd><a href="<?php bloginfo( 'url' ); ?>/contact/#link03">よくあるご質問</a></dd>
							</dl>
						</li>
					</ul>
				</div>
			</div>
		<?php wp_footer(); ?>
		</footer>
