<?php

remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'start_post_rel_link');
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head',10,0);
remove_action('wp_head', 'wp_shortlink_wp_head',10,0);
remove_action('wp_head', 'feed_links',2);
remove_action('wp_head', 'feed_links_extra',3);

function delete_admin_bar() {
	return false;
}
add_filter('show_admin_bar','delete_admin_bar');

function mysetup() {
	add_theme_support( 'post-thumbnails' );
}
add_action( 'after_setup_theme', 'mysetup' );

function SearchFilter($query) {
	if ($query->is_search) {
		$query->set('post_type', 'post');
	}
	return $query;
}
add_filter('pre_get_posts','SearchFilter');

add_action('wp_head', 'get_current_category');

function get_current_category() {
	global $_curcat;
	$cate = null;
	if( is_category() ) {
		$now_cate = get_query_var('cat');
		$cate = get_category($now_cate);
	} else if (is_single() ) {
		$cates = get_the_category();
		$i = 0;
		$use_category = 0;
		foreach ($cates as $cate) {
			if($cate->category_parent > 0 && $use_category == 0) {
				$use_category = $i;
			}
			$i++;
		}
		$cate = $cates[$use_category];
	}
	$_curcat = $cate;
	return $cate;
}


function pagination($pages = '', $range = 4) {
	$showitems = ($range * 2)+1; 

	global $paged;
	if(empty($paged)) $paged = 1;

	if($pages == '') {
		global $wp_query;
		$pages = $wp_query->max_num_pages;
		if(!$pages) {
			$pages = 1;
		}
	}  

	if(1 != $pages) {
		echo "<div class=\"wp-pagenavi\"><span class=\"pages\">".$paged." / ".$pages."</span>";
		if($paged > 1) echo "<a href=\"".get_pagenum_link($paged - 1)."\" class=\"previouspostslink\">&laquo;</a>";
		for ($i=1; $i <= $pages; $i++) {
			if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )) {
				echo ($paged == $i)? "<span class=\"current\">".$i."</span>":"<a href='".get_pagenum_link($i)."' class=\"page larger\">".$i."</a>";
			}
		}
		if ($paged < $pages) echo "<a href=\"".get_pagenum_link($paged + 1)."\" class=\"nextpostslink\">&raquo;</a>";
		echo "</div>\n";
	}
}

?>
