		<header id="header">
			<h1><a href="/"><img src="<?php bloginfo( 'url' ); ?>/common/img/header_logo.jpg" alt="アンナキャムパートナーズ" width="150" height="65"></a></h1>
			<p>当社はカンボジア投資（金融・不動産等）やカンボジア事業進出に関するアドバイザーです。</p>
			<dl>
				<dt><img src="<?php bloginfo( 'url' ); ?>/common/img/header_fontsize.gif" alt="文字サイズ" width="60" height="12"></dt>
				<dd id="sizeM"><a href="javascript:void(0);" onclick="setCss('size_m.css');" onkeypress="setCss('size_m.css');">中</a></dd>
				<dd id="sizeL"><a href="javascript:void(0);" onclick="setCss('size_l.css');" onkeypress="setCss('size_l.css');">大</a></dd>
			</dl>
			<script>
				(function() {
				  var cx = '002953033011611586440:0y0qmlfbgku';
				  var gcse = document.createElement('script'); gcse.type = 'text/javascript';
				  gcse.async = true;
				  gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
					  '//www.google.co.jp/cse/cse.js?cx=' + cx;
				  var s = document.getElementsByTagName('script')[0];
				  s.parentNode.insertBefore(gcse, s);
				})();
			</script>
			<gcse:search></gcse:search>
		</header>
		<nav id="gNav">
			<ul>
				<li class="gNav01">
					<a href="<?php bloginfo( 'url' ); ?>/">ホーム</a>
				</li>
				<li class="gNav02">
					<a href="<?php bloginfo( 'url' ); ?>/company/">会社案内</a>
					<div>
						<ul>
							<li><a href="<?php bloginfo( 'url' ); ?>/company/#link01">弊社のミッション</a></li>
							<li><a href="<?php bloginfo( 'url' ); ?>/company/#link02">会社概要</a></li>
							<li><a href="<?php bloginfo( 'url' ); ?>/company/#link03">メンバー紹介</a></li>
							<li><a href="<?php bloginfo( 'url' ); ?>/company/#link04">アクセスマップ</a></li>
							<li><a href="<?php bloginfo( 'url' ); ?>/company/#link06">提携会社について</a></li>
							<li><a href="<?php bloginfo( 'url' ); ?>/company/#link07">お問い合わせ後の流れ</a></li>
						</ul>
					</div>
				</li>
				<li class="gNav03">
					<a href="<?php bloginfo( 'url' ); ?>/why/">なぜカンボジア</a>
					<div>
						<ul>
							<li><a href="<?php bloginfo( 'url' ); ?>/why/#link01">概況</a></li>
							<li><a href="<?php bloginfo( 'url' ); ?>/why/#link02">進出メリット</a></li>
							<li><a href="<?php bloginfo( 'url' ); ?>/why/#link03">投資メリット</a></li>
							<li><a href="<?php bloginfo( 'url' ); ?>/why/#link04">アジア諸国との比較</a></li>
							<li><a href="<?php bloginfo( 'url' ); ?>/why/#link05">関連リンク集</a></li>
						</ul>
					</div>
				</li>
				<li class="gNav04">
					<a href="<?php bloginfo( 'url' ); ?>/advance/">進出支援</a>
					<div>
						<ul>
							<li><a href="<?php bloginfo( 'url' ); ?>/advance/#link01">法人設立支援</a></li>
							<li><a href="<?php bloginfo( 'url' ); ?>/advance/#link02">事業支援</a></li>
							<li><a href="<?php bloginfo( 'url' ); ?>/advance/#link03">アドバイザー業務</a></li>
							<li><a href="<?php bloginfo( 'url' ); ?>/advance/#link04">資金支援</a></li>
							<li><a href="<?php bloginfo( 'url' ); ?>/advance/#link05">視察ツアー情報</a></li>
							<li><a href="<?php bloginfo( 'url' ); ?>/advance/#link06">カンボジア進出支援事例</a></li>
						</ul>
					</div>
				</li>
				<li class="gNav05">
					<a href="<?php bloginfo( 'url' ); ?>/investment/">投資情報</a>
					<div>
						<ul>
							<li><a href="<?php bloginfo( 'url' ); ?>/investment/#link01">カンボジア投資の魅力</a></li>
							<li><a href="<?php bloginfo( 'url' ); ?>/investment/#link02">金融市場情報</a></li>
							<li><a href="<?php bloginfo( 'url' ); ?>/investment/#link03">不動産情報</a></li>
							<li><a href="<?php bloginfo( 'url' ); ?>/investment/#link04">M&amp;A情報</a></li>
						</ul>
					</div>
				</li>
				<li class="gNav06">
					<a href="<?php bloginfo( 'url' ); ?>/contact/">お問い合わせ</a>
					<div>
						<ul>
							<li><a href="<?php bloginfo( 'url' ); ?>/contact/#link01">お問い合わせ</a></li>
							<li><a href="<?php bloginfo( 'url' ); ?>/contact/#link02">メールマガジン登録</a></li>
							<li><a href="<?php bloginfo( 'url' ); ?>/contact/#link03">よくあるご質問</a></li>
						</ul>
					</div>
				</li>
			</ul>
		</nav>
