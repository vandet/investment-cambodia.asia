	<head>
		<meta charset="utf-8">
<?php if (is_home()) : ?>
		<title><?php bloginfo('name'); ?></title>
<?php else : ?>
		<title><?php wp_title('│', true, 'right'); ?><?php bloginfo('name'); ?></title>
<?php endif; ?>
		<meta name="keywords" content="カンボジア,アンナキャムパートナーズ,不動産,annacam,海外進出,投資,資産運用">
		<meta name="description" content="<?php bloginfo( 'description' ); ?>">
		<!--[if IE]><meta http-equiv="imagetoolbar" content="no"><![endif]-->
		<!--[if lte IE 8]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
		<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'url' ); ?>/common/css/common.css">
		<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'url' ); ?>/common/css/size_m.css" id="sizeChange">
		<?php wp_head(); ?>
		<script src="<?php bloginfo( 'url' ); ?>/common/js/jquery.js"></script>
		<script src="<?php bloginfo( 'url' ); ?>/common/js/jquery.cookie.js"></script>
		<script src="<?php bloginfo( 'url' ); ?>/common/js/common.js"></script>
<?php if (is_page('company')) : ?>
		<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
		<script>
			function initialize() {
				var myLatlng = new google.maps.LatLng(11.554872,104.926525);
				var mapOptions = {
					zoom: 13,
					center: myLatlng
				};

				var map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);

				var contentString = '<p><img src="<?php bloginfo( 'url' ); ?>/common/img/logo_map.jpg" alt="AnnaCam Partners"></p>';

				var infowindow = new google.maps.InfoWindow({
					content: contentString
				});

				var marker = new google.maps.Marker({
					position: myLatlng,
					map: map,
					title: 'AnnaCam Partners'
				});
				infowindow.open(map, marker);
				google.maps.event.addListener(marker, 'click', function() {
					infowindow.open(map,marker);
				});
			}
			google.maps.event.addDomListener(window, 'load', initialize);
		</script>
<?php endif; ?>
		<meta name="google-site-verification" content="hv6MpzzGn9dQayZTp-jsjUh3Q1O0DjhdfDv78I6OZEI" />
	</head>
