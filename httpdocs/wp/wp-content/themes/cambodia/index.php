<!DOCTYPE HTML>
<html>
<?php get_header(); ?>
	<body class="home">
<?php include (TEMPLATEPATH . '/head.php'); ?>
		<section id="topImg">
			<ul>
				<li><a href="<?php bloginfo( 'url' ); ?>/investment/"><img src="<?php bloginfo( 'url' ); ?>/common/img/top_img_05.jpg" alt="「グローバルマインド」と「長期運用」で分散投資をし、資産を創成するという考え方" width="1000" height="430"></a></li>
				<li><a href="<?php bloginfo( 'url' ); ?>/investment/"><img src="<?php bloginfo( 'url' ); ?>/common/img/top_img_04.jpg" alt="安心な不動産投資が可能な環境" width="1000" height="430"></a></li>
				<li><a href="<?php bloginfo( 'url' ); ?>/investment/#link02"><img src="<?php bloginfo( 'url' ); ?>/common/img/top_img_03.jpg" alt="米ドル投資という大きなメリット" width="1000" height="430"></a></li>
				<li><a href="<?php bloginfo( 'url' ); ?>/why/"><img src="<?php bloginfo( 'url' ); ?>/common/img/top_img_02.jpg" alt="「カンボジア」が高成長を続ける知らざれる魅力とは" width="1000" height="430"></a></li>
				<li><img src="<?php bloginfo( 'url' ); ?>/common/img/top_img_01.jpg" alt="東南アジア投資のラストリゾート「カンボジア」" width="1000" height="430"></a></li>
			</ul>
		</section>
		<div id="towColumn">
			<div id="leftColumn">
				<section class="news">
					<h2><img src="<?php bloginfo( 'url' ); ?>/common/img/title_middle03left.png" alt="What's New" width="580" height="30"><a href="<?php bloginfo( 'url' ); ?>/news/"><img src="<?php bloginfo( 'url' ); ?>/common/img/title_middle03right.png" alt="What's New一覧" width="120" height="30" class="rollover"></a></h2>
					<dl>
<?php query_posts($query_string . '&' .'&posts_per_page=8&orderby=date');
if (have_posts()) : while (have_posts()) : the_post(); ?>
						<dt><?php echo get_the_date('Y-m-d'); ?></dt>
						<dd><a href="<?php the_permalink(); ?>"><?php if(mb_strlen( $post->post_title ) < 50) { echo $post->post_title; } else { echo mb_substr($post->post_title, 0, 50).'…';} ?></a></dd>
<?php endwhile; endif; wp_reset_query(); ?>
					</dl>
				</section>
				<section class="business">
					<h2><img src="<?php bloginfo( 'url' ); ?>/common/img/title_middle02.gif" alt="当社がカンボジアで取り組んでいる主な事業" width="700" height="30"></h2>
					<ul class="clearfix">
						<li>
							<h3><a href="<?php bloginfo( 'url' ); ?>/investment/#link03">不動産仲介</a></h3>
							<p>不動産仲介/売買</p>
						</li>
						<li>
							<h3><a href="<?php bloginfo( 'url' ); ?>/investment/#link04">進出支援</a></h3>
							<p>アドバイザー/視察</p>
						</li>
						<li class="right">
							<h3><a href="<?php bloginfo( 'url' ); ?>/seminar/#link01">セミナー情報</a></h3>
							<p>不動産投資セミナー/物件視察</p>
						</li>
						<li class="right">
							<!-- <p><a href="<?php bloginfo( 'url' ); ?>/company/#link02"><img src="<?php bloginfo( 'url' ); ?>/common/img/top_reason_banner.jpg" alt="当社の主な業務と取り組んでいる業務" width="220" height="120" class="rollover"></a></p> -->
						</li>
					</ul>
				</section>
				<section class="reason">
					<h2><img src="<?php bloginfo( 'url' ); ?>/common/img/title_middle01.gif" alt="カンボジアが熱い理由" width="700" height="30"></h2>
					<p>カンボジアと言うと「ポルポト派・地雷」といったイメージで日本には、あまりなじみがない国かもしれませんが、<br>ラオス・ミャンマーと並び、東南アジアの最後の急成長国の一つです。</p>
					<p>しかも他の東南アジアにおいては、外資系への参入規制や、資金移動の際の規制がありますが、<br>カンボジアは規制緩和が進み、こういった規制がありません。</p>
					<p>現在は、人口が拡大していることと、政治的にも安定していることから今後も安定的な成長が見込める有望な市場と位置づけられています。</p>
				</section>
			</div>
<?php get_sidebar(); ?>
		</div>
<?php get_footer(); ?>
<?php include (TEMPLATEPATH . '/google-code.php'); ?>
	</body>
</html>
