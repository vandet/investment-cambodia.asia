<!DOCTYPE HTML>
<html>
<?php get_header(); ?>
	<body class="home">
<?php include (TEMPLATEPATH . '/head.php'); ?>
		<section id="topImg">
			<ul>
				<li><a href="<?php bloginfo( 'url' ); ?>/investment/"><img src="<?php bloginfo( 'url' ); ?>/common/img/top_img_03.jpg" alt="「投資情報」海外投資をお考えの皆様、いまカンボジアが熱い！高利回りでかつドル建て投資が可能な魅力的なカンボジア投資" width="1000" height="430"></a></li>
				<li><a href="<?php bloginfo( 'url' ); ?>/advance/"><img src="<?php bloginfo( 'url' ); ?>/common/img/top_img_02.jpg" alt="「進出支援」中小企業の皆様、円高のいまこそ海外進出のチャンス" width="1000" height="430"></a></li>
				<li><a href="<?php bloginfo( 'url' ); ?>/why/"><img src="<?php bloginfo( 'url' ); ?>/common/img/top_img_01.jpg" alt="「なぜカンボジア」なのか高成長を続ける知られざるアジア最後の新興国" width="1000" height="430"></a></li>
			</ul>
		</section>
		<div id="towColumn">
			<div id="leftColumn">
				<section class="reason">
					<h2><img src="<?php bloginfo( 'url' ); ?>/common/img/title_middle01.gif" alt="カンボジアが熱い理由" width="700" height="30"></h2>
					<p>カンボジアと言うと「ポルポト派・地雷」といったイメージで日本には、あまりなじみがない国かもしれませんが、<br>ラオス・ミャンマーと並び、東南アジアの最後の急成長国の一つです。</p>
					<p>しかも他の東南アジアにおいては、外資系への参入規制や、資金移動の際の規制がありますが、<br>カンボジアは規制緩和が進み、こういった規制がありません。</p>
					<p>現在は、人口が拡大していることと、政治的にも安定していることから今後も安定的な成長が見込める有望な市場と位置づけられています。</p>
				</section>
				<section class="business">
					<h2><img src="<?php bloginfo( 'url' ); ?>/common/img/title_middle02.gif" alt="当社がカンボジアで取り組んでいる主な事業" width="700" height="30"></h2>
					<ul class="clearfix">
						<li>
							<h3><a href="<?php bloginfo( 'url' ); ?>/advance/#link01">法人設立支援</a></h3>
							<p>法人設立/アドバイザー/視察ツアー/事業支援/資金支援/住居等の生活支援</p>
						</li>
						<li>
							<h3><a href="<?php bloginfo( 'url' ); ?>/advance/#link02">事業支援</a></h3>
							<p>会計/税務/レンタルオフィス/人材紹介/Web関連業務/事業コンサルティング</p>
						</li>
						<li class="right">
							<h3><a href="<?php bloginfo( 'url' ); ?>/advance/#link04">資金支援</a></h3>
							<p>進出及び進出後の事業展開のサポート</p>
						</li>
						<li>
							<h3><a href="<?php bloginfo( 'url' ); ?>/investment/#link02">金融市場情報</a></h3>
							<p>カンボジア証券相場/主要金融機関預金金利</p>
						</li>
						<li>
							<h3><a href="<?php bloginfo( 'url' ); ?>/investment/#link03">不動産情報</a></h3>
							<p>不動産情報/プノンペンの優良コンドミニアム等</p>
						</li>
						<li class="right">
							<p><a href="<?php bloginfo( 'url' ); ?>/company/#link02"><img src="<?php bloginfo( 'url' ); ?>/common/img/top_reason_banner.jpg" alt="当社の主な業務と取り組んでいる業務" width="220" height="120" class="rollover"></a></p>
						</li>
					</ul>
				</section>
				<section class="news">
					<h2><img src="<?php bloginfo( 'url' ); ?>/common/img/title_middle03left.gif" alt="お知らせ" width="580" height="30"><a href="<?php bloginfo( 'url' ); ?>/news/"><img src="<?php bloginfo( 'url' ); ?>/common/img/title_middle03right.gif" alt="お知らせ一覧" width="120" height="30" class="rollover"></a></h2>
					<dl>
<?php query_posts($query_string . '&' .'&posts_per_page=5&orderby=date');
if (have_posts()) : while (have_posts()) : the_post(); ?>
						<dt><?php echo get_the_date('Y-m-d'); ?></dt>
						<dd><a href="<?php the_permalink(); ?>"><?php if(mb_strlen( $post->post_title ) < 50) { echo $post->post_title; } else { echo mb_substr($post->post_title, 0, 50).'…';} ?></a></dd>
<?php endwhile; endif; wp_reset_query(); ?>
					</dl>
				</section>
			</div>
<?php get_sidebar(); ?>
		</div>
<?php get_footer(); ?>
<?php include (TEMPLATEPATH . '/google-code.php'); ?>
	</body>
</html>
