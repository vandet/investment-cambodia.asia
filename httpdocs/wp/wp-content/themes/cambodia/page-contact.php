<?php

define("SUBJECT", "お問い合わせフォーム");

define("SUBJECT2", "お問い合わせありがとうございます【アンナキャムパートナーズ】");

define("SUBJECTM01", "メールマガジン登録フォーム");

define("SUBJECT2M01", "メールマガジン登録通知【アンナキャムパートナーズ】");

define("SUBJECTM02", "メールマガジン解除フォーム");

define("SUBJECT2M02", "メールマガジン解除通知【アンナキャムパートナーズ】");

define("FROMEMAIL", "investment-cambodia.asia");

define("MAILTO", "info@annacampartners.com");


$jcode = '1';
$charset = array();
$charset[1] = 'UTF-8';


$ckurl = "";

switch($_POST['mode']):
	case 'do_form1':
		decode();
		check();
		check_html();
		break;
	case 'do_form2':
		decode();
		sendmail();
		finish_html();
		break;
	case 'do_form_m1':
		decode();
		check01();
		sendmail01();
		finish01_html();
		break;
	case 'do_form_m2':
		decode();
		check02();
		sendmail02();
		finish02_html();
		break;
	default:
		top_html();
		break;
endswitch;
exit();


function top_html(){

	include (TEMPLATEPATH . '/contact_form.php');

}

function check_html(){
	
	include (TEMPLATEPATH . '/contact_confirm.php');
	
}


function check() {

	$errmsg = "";
	if(!$_POST['nameA']){
		$errmsg .= "［氏名（漢字）］は必須項目です。<br>";
	}
	if(!$_POST['nameB']){
		$errmsg .= "［氏名（カタカナ）］は必須項目です。<br>";
	}
	if(!$_POST['com']){
		$errmsg .= "［会社名］は必須項目です。<br>";
	}
	if(!$_POST['unit']){
		$errmsg .= "［部署名］は必須項目です。<br>";
	}
	if(!$_POST['telCom']){
		$errmsg .= "［電話番号（会社）］は必須項目です。<br>";
	}
	if(!$_POST['telMobile']){
		$errmsg .= "［電話番号（携帯）］は必須項目です。<br>";
	}
	if(!$_POST['mailA']){
		$errmsg .= "［メールアドレス］は必須項目です。<br>";
	}elseif(!preg_match('/[\w.-]+\@[\w.-]+\.[a-zA-Z]{2,3}/', $_POST['mailA'])){
		$errmsg .= "［メールアドレス］の入力内容が正しくありません。<br>";
	}
	if(!$_POST['mailB']){
		$errmsg .= "［メールアドレス（確認）］は必須項目です。<br>";
	}elseif(!preg_match('/[\w.-]+\@[\w.-]+\.[a-zA-Z]{2,3}/', $_POST['mailB'])){
		$errmsg .= "［メールアドレス（確認）］の入力内容が正しくありません。<br>";
	}elseif($_POST['mailA'] != $_POST['mailB']){
		$errmsg .= "［メールアドレス］と［メールアドレス（確認）］の内容が異なります。<br>";
	}
	if(!$_POST['cts']){
		$errmsg .= "［お問い合わせ及び、ご希望のサービス内容の記載］は必須項目です。<br>";
	}
	
	
	if($errmsg) {
		error($errmsg);
	}

}


function decode() {

	if ($_SERVER['REQUEST_METHOD'] == "GET") {	error("不正な投稿です");	}

	foreach ($_POST as $key => $value) {
		if (get_magic_quotes_gpc()) { $value = stripslashes($value); }

		if($_POST['mode'] == "do_form1"){
			$characterCode = "UTF-8";
			$code = mb_detect_encoding($value); 
			if ($code != $characterCode) {
			    $value = mb_convert_encoding($value, $characterCode);
			}
			$value = mb_convert_kana($value, "aKV", $characterCode);

			$value = htmlspecialchars($value);

			$value = str_replace(",", "，", $value);

			if($key == "cts"){
				$value = str_replace("\r\n", "<br>", $value);
				$value = str_replace("\r", "<br>", $value);
				$value = str_replace("\n", "<br>", $value);
			}

		}elseif($_POST['mode'] == "do_form2"){
			$value = tag_text($value);
		}elseif(($_POST['mode'] == "do_form_m1") ||($_POST['mode'] == "do_form_m2")){
			$characterCode = "UTF-8";
			$code = mb_detect_encoding($value); 
			if ($code != $characterCode) {
			    $value = mb_convert_encoding($value, $characterCode);
			}
			$value = mb_convert_kana($value, "aKV", $characterCode);

			$value = htmlspecialchars($value);

			$value = str_replace(",", "，", $value);
		}
		$value = trim($value);

		$_POST[$key] = $value;
	}
}

function sendmail(){
	global $charset,$jcode;

	$date = gmdate("Y年m月d日 H:i:s",time()+9*60*60);

$mailbody = <<<EOM

$date
お問い合わせフォームからの投稿がありました。
下記の内容になります。

◇氏名（漢字）
${_POST['nameA']}

◇氏名（カタカナ）
${_POST['nameB']}

◇会社名
${_POST['com']}

◇役職
${_POST['post']}

◇部署名
${_POST['unit']}

◇電話番号（会社）
${_POST['telCom']}

◇電話番号（携帯）
${_POST['telMobile']}

◇メールアドレス
${_POST['mailA']}

◇ご希望のご連絡手段
${_POST['means']}

◇お問い合わせ及び、ご希望のサービス内容
${_POST['cts']}

EOM;

$mailbody2 = <<<EOM

◆お問い合わせ自動返信メール

${_POST['nameA']}様
この度はお問い合わせを頂きまして誠にありがとうございました。
担当者よりメールにてご連絡を差し上げますので、
今しばらくお待ちくださいませ。

【お問い合わせ内容】

◇氏名（漢字）
${_POST['nameA']}

◇氏名（カタカナ）
${_POST['nameB']}

◇会社名
${_POST['com']}

◇役職
${_POST['post']}

◇部署名
${_POST['unit']}

◇電話番号（会社）
${_POST['telCom']}

◇電話番号（携帯）
${_POST['telMobile']}

◇メールアドレス
${_POST['mailA']}

◇ご希望のご連絡手段
${_POST['means']}

◇お問い合わせ及び、ご希望のサービス内容
${_POST['cts']}

もし1週間経っても連絡が無い場合、
ご登録が出来ていない可能性がございますので
お手数でございますが、下記までご連絡頂けますと幸いです。

-------------------------------------
アンナキャムパートナーズ株式会社
-------------------------------------

EOM;


	mb_language("japanese");
	mb_internal_encoding("$charset[$jcode]");
	$headers = "From: ".FROMEMAIL."<".MAILTO.">\r\n";
	$emailto = $_POST['mailA'];
	$rcode = @mb_send_mail(MAILTO, SUBJECT, $mailbody, $headers, '-f'.MAILTO);
	$rcode2 = @mb_send_mail($emailto, SUBJECT2, $mailbody2, $headers, '-f'.MAILTO);

	if(!$rcode){	error("メール送信エラー");	}
	if(!$rcode2){	error("メール送信エラー");	}

}


function finish_html(){
	global $charset,$jcode;
	
	include (TEMPLATEPATH . '/contact_thanks.php');
	
}


function error($msg){
	global $charset,$jcode;

	include (TEMPLATEPATH . '/contact_error.php');

}


function check01() {

	$errmsg = "";
	if(!$_POST['mail01A']){
		$errmsg .= "［メールアドレス］は必須項目です。<br>";
	}elseif(!preg_match('/[\w.-]+\@[\w.-]+\.[a-zA-Z]{2,3}/', $_POST['mail01A'])){
		$errmsg .= "［メールアドレス］の入力内容が正しくありません。<br>";
	}
	if(!$_POST['mail01B']){
		$errmsg .= "［メールアドレス確認用］は必須項目です。<br>";
	}elseif(!preg_match('/[\w.-]+\@[\w.-]+\.[a-zA-Z]{2,3}/', $_POST['mail01B'])){
		$errmsg .= "［メールアドレス確認用］の入力内容が正しくありません。<br>";
	}elseif($_POST['mail01A'] != $_POST['mail01B']){
		$errmsg .= "［メールアドレス］と［メールアドレス確認用］の内容が異なります。<br>";
	}
	
	
	if($errmsg) {
		error01($errmsg);
	}

}

function check02() {

	$errmsg = "";
	if(!$_POST['mail02A']){
		$errmsg .= "［メールアドレス］は必須項目です。<br>";
	}elseif(!preg_match('/[\w.-]+\@[\w.-]+\.[a-zA-Z]{2,3}/', $_POST['mail02A'])){
		$errmsg .= "［メールアドレス］の入力内容が正しくありません。<br>";
	}
	if(!$_POST['mail02B']){
		$errmsg .= "［メールアドレス確認用］は必須項目です。<br>";
	}elseif(!preg_match('/[\w.-]+\@[\w.-]+\.[a-zA-Z]{2,3}/', $_POST['mail02B'])){
		$errmsg .= "［メールアドレス確認用］の入力内容が正しくありません。<br>";
	}elseif($_POST['mail02A'] != $_POST['mail02B']){
		$errmsg .= "［メールアドレス］と［メールアドレス確認用］の内容が異なります。<br>";
	}
	if(!$_POST['cts02']){
		$errmsg .= "［解除理由］は必須項目です。<br>";
	}
	
	
	if($errmsg) {
		error02($errmsg);
	}

}


function sendmail01(){
	global $charset,$jcode;

	$date = gmdate("Y年m月d日 H:i:s",time()+9*60*60);

$mailbody_m01 = <<<EOM

$date
メルマガ登録フォームからの投稿がありました。
下記の内容になります。

◇メールアドレス
${_POST['mail01A']}

EOM;

$mailbody2_m01 = <<<EOM

◆メルマガ登録自動返信メール

この度は登録をして頂きまして誠にありがとうございました。

【ご登録情報】
${_POST['mail01A']}

[お願い]
上記の無料メールのお申し込み手続きをした覚えがない場合、誤登録の可能性があります。
このような場合は、お手数ですが下記より登録の取り消しをしていただきますようお願いいたします。

http://investment-cambodia.asia/contact/

-------------------------------------
アンナキャムパートナーズ株式会社
-------------------------------------

EOM;


	mb_language("japanese");
	mb_internal_encoding("$charset[$jcode]");
	$headers = "From: ".FROMEMAIL."<".MAILTO.">\r\n";
	$emailto = $_POST['mail01A'];
	$rcode = @mb_send_mail(MAILTO, SUBJECTM01, $mailbody_m01, $headers, '-f'.MAILTO);
	$rcode2 = @mb_send_mail($emailto, SUBJECT2M01, $mailbody2_m01, $headers, '-f'.MAILTO);

	if(!$rcode){	error("メール送信エラー");	}
	if(!$rcode2){	error("メール送信エラー");	}

}


function sendmail02(){
	global $charset,$jcode;

	$date = gmdate("Y年m月d日 H:i:s",time()+9*60*60);

$mailbody_m02 = <<<EOM

$date
メルマガ解除フォームからの投稿がありました。
下記の内容になります。

◇メールアドレス
${_POST['mail02A']}

◇解除理由
${_POST['cts02']}

EOM;

$mailbody2_m02 = <<<EOM

◆メルマガ解除自動返信メール

今までご拝読頂きまして誠にありがとうございました。
今後ともラ・アトレ（カンボジア）をどうぞ宜しくお願い致します。

-------------------------------------
アンナキャムパートナーズ株式会社
-------------------------------------

EOM;


	mb_language("japanese");
	mb_internal_encoding("$charset[$jcode]");
	$headers = "From: ".FROMEMAIL."<".MAILTO.">\r\n";
	$emailto = $_POST['mail02A'];
	$rcode = @mb_send_mail(MAILTO, SUBJECTM02, $mailbody_m02, $headers, '-f'.MAILTO);
	$rcode2 = @mb_send_mail($emailto, SUBJECT2M02, $mailbody2_m02, $headers, '-f'.MAILTO);

	if(!$rcode){	error("メール送信エラー");	}
	if(!$rcode2){	error("メール送信エラー");	}

}


function finish01_html(){
	global $charset,$jcode;
	
	include (TEMPLATEPATH . '/contact_mail01.php');
	
}


function finish02_html(){
	global $charset,$jcode;
	
	include (TEMPLATEPATH . '/contact_mail02.php');
	
}


function error01($msgM01){
	global $charset,$jcode;

	include (TEMPLATEPATH . '/contact_error01.php');

}


function error02($msgM02){
	global $charset,$jcode;

	include (TEMPLATEPATH . '/contact_error02.php');

}


function tag_text($str){
	$str = str_replace("&amp;", "&", $str);
	$str = str_replace("&quot;", "\"", $str);
	$str = str_replace("&#39;", "\'", $str);
	$str = str_replace("&lt;", "<", $str);
	$str = str_replace("&gt;", ">", $str);
	$str = str_replace("&#44;", ",", $str);
	$str = str_replace("<br>", "\n", $str);
	return $str;
}

?>
