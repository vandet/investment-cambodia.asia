<!DOCTYPE HTML>
<html>
<?php get_header(); ?>
<?php if (is_page('company')) : ?>
	<body class="company">
<?php elseif (is_page('why')) : ?>
	<body class="why">
<?php elseif (is_page('investment')) : ?>
	<body class="investment">
	<?php elseif (is_page('seminar')) : ?>
	<body class="seminar">
<?php elseif (is_page('contact','contact_form','contact_confirm','contact_error','contact_error01','contact_error02','contact_mail01','contact_mail02','contact_thanks')) : ?>
	<body class="contact">
<?php elseif (is_page('privacy')) : ?>
	<body class="other">
<?php elseif (is_page('sitemap')) : ?>
	<body class="other">
<?php endif; ?>
<?php include (TEMPLATEPATH . '/head.php'); ?>
		<div id="pageTitle">
<?php if (is_page('company')) : ?>
			<h2><img src="<?php bloginfo( 'url' ); ?>/common/img/pagetitle02.gif" alt="会社案内 Company Profile" width="110" height="40"></h2>
<?php elseif (is_page('why')) : ?>
			<h2><img src="<?php bloginfo( 'url' ); ?>/common/img/pagetitle03.gif" alt="なぜカンボジア Why Cambodia" width="140" height="40"></h2>
<?php elseif (is_page('investment')) : ?>
			<h2><img src="<?php bloginfo( 'url' ); ?>/common/img/pagetitle04.png" alt="投資/進出情報 Investment/Advance Support" width="150" height="40"></h2>
<?php elseif (is_page('seminar')) : ?>
			<h2><img src="<?php bloginfo( 'url' ); ?>/common/img/pagetitle05.png" alt="セミナー情報 Investment Information" width="150" height="40"></h2>
<?php elseif (is_page('contact','contact_form','contact_confirm','contact_error','contact_error01','contact_error02','contact_mail01','contact_mail02','contact_thanks')) : ?>
			<h2><img src="<?php bloginfo( 'url' ); ?>/common/img/pagetitle06.gif" alt="お問い合わせ Contact" width="120" height="40"></h2>
<?php elseif (is_page('privacy')) : ?>
			<h2><img src="<?php bloginfo( 'url' ); ?>/common/img/pagetitle07.gif" alt="プライバシーポリシー Privacy Policy" width="197" height="40"></h2>
<?php elseif (is_page('sitemap')) : ?>
			<h2><img src="<?php bloginfo( 'url' ); ?>/common/img/pagetitle08.gif" alt="サイトマップ Site Map" width="120" height="40"></h2>
<?php endif; ?>
		</div>
		<div id="towColumn">
			<div id="leftColumn">
