<!DOCTYPE HTML>
<html>
<?php get_header(); ?>
	<body class="other">
<?php include (TEMPLATEPATH . '/head.php'); ?>
		<div id="pageTitle">
			<h2>検索結果</h2>
		</div>
		<div id="towColumn">
			<div id="leftColumn">
<?php if (have_posts()) : ?>
				<p class="number">「<?php echo wp_specialchars($s, 1); ?>」で検索検索した結果：<?php echo $wp_query->found_posts; ?>件</p>
				<article class="newsList">
					<dl>
<?php while (have_posts()) : the_post(); ?>
						<dt><?php echo get_the_date('Y-m-d'); ?></dt>
						<dd><a href="<?php the_permalink(); ?>"><?php if(mb_strlen( $post->post_title ) < 50) { echo $post->post_title; } else { echo mb_substr($post->post_title, 0, 50).'…';} ?></a></dd>
<?php endwhile; ?>
					</dl>
				</article>
<?php else : ?>
				<p class="number">キーワードに一致する情報は見つかりませんでした。</p>
<?php endif; ?>
			</div>
			<div id="rightColumn">
				<aside>
					<section class="contactBox contactUnder clearfix">
						<p><img src="<?php bloginfo( 'url' ); ?>/common/img/right_contact.gif" alt="お問い合わせ +855 12 215 240 土日祝日を除く9:00～18:00" width="250" height="145"></p>
						<p class="mail"><a href="<?php bloginfo( 'url' ); ?>/contact/"><img src="<?php bloginfo( 'url' ); ?>/common/img/right_contact_mail.jpg" alt="お問い合わせフォーム" width="230" height="45" class="rollover"></a></p>
					</section>
				</aside>
			</div>
		</div>
<?php get_footer(); ?>
<?php include (TEMPLATEPATH . '/google-code.php'); ?>
	</body>
</html>
