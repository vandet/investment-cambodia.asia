<!DOCTYPE HTML>
<html>
<?php get_header(); ?>
	<body class="other">
<?php include (TEMPLATEPATH . '/head.php'); ?>
		<div id="pageTitle">
			<h2><img src="<?php bloginfo( 'url' ); ?>/common/img/pagetitle01.gif" alt="What's New News" width="80" height="40"></h2>
		</div>
		<div id="towColumn">
			<div id="leftColumn">
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<section class="newsPost">
					<h3><?php the_title(); ?></h3>
					<p class="date"><?php echo get_the_date('Y-m-d'); ?></p>
					<div>
<?php the_content(); ?>
					</div>
				</section>
<?php endwhile; endif; ?>
			</div>
			<div id="rightColumn">
				<nav>
					<dl>
						<dt><span><img src="<?php bloginfo( 'url' ); ?>/common/img/right_title01.png" alt="News" width="250" height="30"></span></dt>
						<dd class="top"><a href="<?php bloginfo( 'url' ); ?>/news/">What's New</a></dd>
					</dl>
				</nav>
				<aside>
					<section class="banner">
						<ul>
							<li><a href="http://annahome.asia" target="_blank"><img src="<?php bloginfo( 'url' ); ?>/common/img/right_banner01.png" alt="Anna Home" width="250" height="90" class="rollover"></a></li>
							<li><a href="http://cambodianna.blogspot.com/" target="_blank"><img src="<?php bloginfo( 'url' ); ?>/common/img/right_banner02.png" alt="ジャパンデスクアンナのカンボジア投資情報" width="250" height="90" class="rollover"></a></li>
						</ul>
					</section>
					<section class="contactBox contactUnder clearfix">
						<p><img src="<?php bloginfo( 'url' ); ?>/common/img/right_contact.gif" alt="お問い合わせ +855 12 215 240 土日祝日を除く9:00～18:00" width="250" height="145"></p>
						<p class="mail"><a href="<?php bloginfo( 'url' ); ?>/contact/"><img src="<?php bloginfo( 'url' ); ?>/common/img/right_contact_mail.jpg" alt="お問い合わせフォーム" width="230" height="45" class="rollover"></a></p>
					</section>
				</aside>
			</div>
		</div>
<?php get_footer(); ?>
<?php include (TEMPLATEPATH . '/google-code.php'); ?>
	</body>
</html>
